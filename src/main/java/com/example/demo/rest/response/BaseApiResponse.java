package com.example.demo.rest.response;

import com.example.demo.pagination.Pagination;
import org.springframework.http.HttpStatus;

import java.security.Timestamp;

public class BaseApiResponse<T> {
    private String message;
    private T data;
    private HttpStatus status;
    private Timestamp time;
    public Pagination pagination;

    public BaseApiResponse() {
    }

    public BaseApiResponse(String message, T data, HttpStatus status, Timestamp time,Pagination pagination) {
        this.message = message;
        this.data = data;
        this.status = status;
        this.time = time;
        this.pagination=pagination;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    @Override
    public String toString() {
        return "BaseApiResponse{" +
                "message='" + message + '\'' +
                ", data=" + data +
                ", status=" + status +
                ", time=" + time +
                ", pagination=" + pagination +
                '}';
    }


}
