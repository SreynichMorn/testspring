package com.example.demo.rest.restController;

import com.example.demo.pagination.Pagination;
import com.example.demo.repository.model.BookModel;
import com.example.demo.rest.request.BookRequestModel;
import com.example.demo.rest.response.BaseApiResponse;
import com.example.demo.service.Implement.BookServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Timestamp;
import java.util.ArrayList;
import java.util.List;


@RestController
public class BookRestController {
    private BookServiceImp bookServiceImp;
    @Autowired
    public void setBookServiceImp(BookServiceImp bookServiceImp) {
        this.bookServiceImp = bookServiceImp;
    }
    @PostMapping("/books")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> insert(@RequestBody BookRequestModel book) {

        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();

        ModelMapper mapper = new ModelMapper();
        BookModel bookModel =mapper.map(book,BookModel.class);

        // Give generated ID to result

        BookModel result = bookServiceImp.insert(bookModel);

        BookRequestModel result2 = mapper.map(result, BookRequestModel.class);

        response.setMessage("You have added book successfully");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
//        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);

    }
    @GetMapping("/books")
    public ResponseEntity<BaseApiResponse<List<BookRequestModel>>> select(@RequestParam(value = "page" , required = false , defaultValue = "1") int page,
                                                                          @RequestParam(value = "limit" , required = false , defaultValue = "2") int limit) {
        Pagination pagination = new Pagination(page, limit);
        pagination.setPageNum(page);
        pagination.setLimit(limit);
        pagination.nextPageNum();
        pagination.previousPageNum();
        pagination.setTotalPageNum(pagination.getTotalPageNum());


        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<List<BookRequestModel>> response = new BaseApiResponse<>();

        List<BookModel> bookModelList = bookServiceImp.select(pagination);
        List<BookRequestModel> book = new ArrayList<>();

        for (BookModel bookModel : bookModelList) {
            book.add(mapper.map(bookModel, BookRequestModel.class));
        }

        response.setPagination(pagination);
        response.setMessage("You have found all book successfully");
        response.setData(book);
        response.setStatus(HttpStatus.OK);


        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/books/{id}")
    public ResponseEntity<Void> delete (@PathVariable("id") int id){
        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();

        bookServiceImp.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PutMapping("/books/{id}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> update(
            @PathVariable("id") int id,
            @RequestBody BookRequestModel bookRequestModel){
        ModelMapper modelMapper=new ModelMapper();
        BookModel model=modelMapper.map(bookRequestModel,BookModel.class);
        BookRequestModel responseModel=modelMapper.map(bookServiceImp.update(id,model),BookRequestModel.class);
        BaseApiResponse<BookRequestModel> response = new BaseApiResponse <>();
        response.setMessage("YOU HAVE UPDATED SUCCESSFULLY!");
        response.setStatus(HttpStatus.OK);
        response.setData(responseModel);

    //    response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }

}
