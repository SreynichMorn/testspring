package com.example.demo.rest.restController;

import com.example.demo.repository.model.CategoryModel;
import com.example.demo.rest.request.CategoryRequestModel;
import com.example.demo.rest.response.BaseApiResponse;
import com.example.demo.service.Implement.CategoryServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;

@RestController
public class CategoryRestController {
    private CategoryServiceImp categoryServiceImp;
    @Autowired
    public void setCategoryServiceImp(CategoryServiceImp categoryServiceImp) {
        this.categoryServiceImp = categoryServiceImp;

    }
    @PostMapping("/category")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> insert(@RequestBody CategoryRequestModel category) {

        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();

        ModelMapper mapper = new ModelMapper();
        CategoryModel categoryModel =mapper.map(category,CategoryModel.class);

        CategoryModel result = categoryServiceImp.insert(categoryModel);

        CategoryRequestModel result2 = mapper.map(result, CategoryRequestModel.class);

        response.setMessage("You have added category successfully");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
//        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);

    }
    @GetMapping("/category")
    public ResponseEntity<BaseApiResponse<List<CategoryRequestModel>>> select() {

        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<List<CategoryRequestModel>> response = new BaseApiResponse<>();

        List<CategoryModel> categoryModelList = categoryServiceImp.select();
        List<CategoryRequestModel> category = new ArrayList<>();

        for (CategoryModel categoryModel :categoryModelList ) {
            category.add(mapper.map(categoryModel, CategoryRequestModel.class));
        }

        response.setMessage("You have found all category successfully");
        response.setData(category);
        response.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/category/{id}")
    public ResponseEntity<Void> delete (@PathVariable("id") int id){
        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();
        categoryServiceImp.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PutMapping("/category/{id}")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> update(
            @PathVariable("id") int id,
            @RequestBody CategoryRequestModel categoryRequestModel){
        ModelMapper modelMapper=new ModelMapper();
        CategoryModel model=modelMapper.map(categoryRequestModel,CategoryModel.class);
        CategoryRequestModel responseModel=modelMapper.map(categoryServiceImp.update(id,model),CategoryRequestModel.class);
        BaseApiResponse<CategoryRequestModel> respone = new BaseApiResponse <>();
        respone.setMessage("YOU HAVE UPDATED SUCCESSFULLY!");
        respone.setStatus(HttpStatus.OK);
        respone.setData(responseModel);
//        respone.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(respone);
    }

}
