package com.example.demo.pagination;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Pagination implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty("pageNum")
    private int pageNum;

    @JsonProperty("limit")
    private int limit;

    @JsonProperty("total_Num")
    private int totalNum;

    @JsonProperty("total_pageNum")
    private int totalPageNum;

    @JsonIgnore
    private int offset;

    public Pagination() {
        this(1, 15, 0, 0);

    }
    public Pagination(int pageNum, int limit){
        this.pageNum = pageNum;
        this.limit = limit;
        this.totalNum = 0;
        this.totalPageNum = 0;
    }

    public Pagination(int pageNum, int limit, int totalNum, int totalPageNum){
        this.pageNum = pageNum;
        this.limit = limit;
        this.totalNum = totalNum;
        this.totalPageNum = totalPageNum;
    }

    public int getPageNum() {
        return pageNum;
    }

    public int totalPageNum(){
        return (int) Math.ceil((double)this.totalNum/limit);

    }

    public int nextPageNum(){
        return this.pageNum+1;
    }

    public int previousPageNum(){
        return this.pageNum-1;
    }

    public boolean hasNextPageNum(){
        return this.nextPageNum() <=this.totalPageNum()? true :false;
    }

    public boolean hasPreviousPage(){
        return this.previousPageNum()>=1 ? true : false;
    }

    public int offset(){
        this.offset = (this.pageNum-1)* limit;
        return this.offset;
    }

    public void setPageNum(int currentPage) {
        this.pageNum = currentPage;
        this.offset();
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalCount(int totalNum) {
        this.totalNum = totalNum;
        this.totalPageNum = (int) totalPageNum();
    }

    public int getTotalPageNum() {
        return totalPageNum;
    }

    public void setTotalPageNum(int totalPageNum) {
        this.totalPageNum = totalPageNum;
    }
    public int getOffset() {
        return offset;
    }


    public void setOffset(int offset) {
        this.offset = offset;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }



}