package com.example.demo.repository;

import com.example.demo.pagination.Pagination;
import com.example.demo.repository.model.BookModel;
import com.example.demo.repository.model.CategoryModel;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface BookRepository {

    @Insert("INSERT INTO tb_books ( title, author, description, thumbnail, category_id)" +
            "VALUES ( #{title}, #{author}, #{description}, #{thumbnail}, #{category.id})")
    boolean insert(BookModel book);

    @Select("SELECT * FROM tb_books  LIMIT #{pagination.limit}  OFFSET #{pagination.offset}")
    @Results({
            @Result(column = "category_id", property = "category", many = @Many(select = "selectAllCategory"))
    })
    List<BookModel> select(@Param("pagination") Pagination pagination);

    @Select("SELECT * FROM tb_categories WHERE id=#{category_id}")
    CategoryModel selectAllCategory(int category_id);

    @Delete("delete FROM tb_books WHERE id =#{id}")
    void delete(int id);


    @Update("UPDATE tb_books SET title=#{bookModel.title},author=#{bookModel.author},description=#{bookModel.description}, thumbnail=#{bookModel.thumbnail}  WHERE id=#{id}")

    boolean update(int id,BookModel  bookModel);
}