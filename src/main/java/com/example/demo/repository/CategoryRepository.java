package com.example.demo.repository;

import com.example.demo.repository.model.BookModel;
import com.example.demo.repository.model.CategoryModel;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @Insert("INSERT INTO tb_categories (title)" + "VALUES ( #{title})")
    boolean insert(CategoryModel category);
    @Select("SELECT * FROM tb_categories")
    List<CategoryModel> select();

    @Delete("delete FROM tb_categories WHERE id =#{id}")
    void delete(int id);

    @Update("UPDATE tb_categories SET title=#{categoryModel.title}  WHERE id=#{id}")
    boolean update(int id,CategoryModel  categoryModel);

}
