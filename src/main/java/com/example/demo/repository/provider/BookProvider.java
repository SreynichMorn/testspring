package com.example.demo.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class BookProvider {
    public String select() {
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
        }}.toString();
    }

    public String selectByFilter(String title) {
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");

        if (title != null || !title.equals(""))
                WHERE("title = #{title}");
            else
                WHERE();

        }}.toString();
    }

}
