package com.example.demo.service;

import com.example.demo.pagination.Pagination;
import com.example.demo.repository.model.BookModel;

import java.util.List;

public interface
BookService {
    BookModel insert(BookModel book);
    List<BookModel> select(Pagination pagination);
    void delete(int id);
    BookModel update(int id,BookModel book);
}
