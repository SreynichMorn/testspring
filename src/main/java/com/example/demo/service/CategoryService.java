package com.example.demo.service;
import com.example.demo.repository.model.CategoryModel;

import java.util.List;

public interface CategoryService {
    CategoryModel insert(CategoryModel category);
    List<CategoryModel> select();
    void delete(int id);
    CategoryModel update(int id,CategoryModel book);
}
