package com.example.demo.service.Implement;

import com.example.demo.pagination.Pagination;
import com.example.demo.repository.BookRepository;
import com.example.demo.repository.model.BookModel;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {
    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public BookModel insert(BookModel book) {
        boolean isInserted =bookRepository.insert(book);
        if (isInserted)
            return book;
        else
            return null;
    }


    @Override
    public List<BookModel> select(Pagination pagination) {
        return bookRepository.select(pagination);
    }

    @Override
    public void delete(int id) {
         bookRepository.delete(id);
    }

    @Override
    public BookModel update(int id, BookModel book) {
        boolean isUpdated = bookRepository.update(id, book);
        if (isUpdated) {
            return book;
        } else {
            return null;
        }
    }



}
