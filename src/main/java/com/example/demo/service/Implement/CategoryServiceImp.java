package com.example.demo.service.Implement;

import com.example.demo.repository.CategoryRepository;
import com.example.demo.repository.model.CategoryModel;
import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {
    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public CategoryModel insert(CategoryModel category) {
        boolean isInserted =categoryRepository.insert(category);
        if (isInserted)
            return category;
        else
            return null;
    }
    @Override
    public List<CategoryModel> select() {
        return categoryRepository.select();
    }

    @Override
    public void delete(int id) {
        categoryRepository.delete(id);
    }


    @Override
    public CategoryModel update(int id, CategoryModel category) {
        boolean isUpdated = categoryRepository.update(id, category);
        if (isUpdated) {
            return category;
        } else {
            return null;
        }
    }
}
